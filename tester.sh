#!/bin/bash

exercise=ex_$1
binary=$exercise.php
output="output.test"

function play_mouli
{
if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cd ./tests/$exercise && php moulinette.php $binary &> "../${exercise}_REF"
else
    sudo -u student php ./tests/$exercise/moulinette.php ./$exercise/$binary &> output.test
    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
}

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

case "$exercise" in
    "ex_11")
	if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
	then
	    cd ./tests/$exercise && php moulinette.php $binary "Hello" "World" "This is very cool" 4242 0x15 "All tests passed ?!" &> "../${exercise}_REF"
	else
	    sudo -u student php ./tests/$exercise/moulinette.php ./$exercise/$binary  "Hello" "World" "This is very cool" 4242 0x15 "All tests passed ?!" &> output.test
	    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
	    
	    if [ $? -ne 0 ]; then
		echo "KO: check your traces below this line..."
		echo "$res"
	    else
		echo "OK"
	    fi
	fi	
    ;;
    *)
	play_mouli $@
    ;;
esac
